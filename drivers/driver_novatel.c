#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "../include/gpsd_config.h"  /* must be before all includes */

#include "../include/bits.h"
#include "../include/gpsd.h"
#include "../include/crc32_oem7.h"

#if defined(NOVATEL_ENABLE) && defined(BINARY_ENABLE)

#include "../include/driver_novatel.h"

/* NovAtel ASCII message structure:
 *
 *   '#' header ';' data field... ',' data field... ',' data field... '*' checksum '\r' '\n'
 *
 * - Only ASCII characters between 32 and 127 inclusive
 * - Checksum is always 8 hexadecimal characters
 *
 * NovAtel Abbreviated ASCII structure
 *
 *   '<' human readable text  '\r' '\n'
 *
 * NovAtel Binary structure:
 *
 *   0xAA 0x44 0x12 (header) (message) (checksum)
 *
 * Header and message length are embedded in header data. Checksum is 4 bytes in length.
 */


#define BIT(x) (1U << (x))

static const size_t header_length = 28;
static const size_t checksum_length = sizeof(uint32_t);

/* forward declaration */
static bool novatel_speed_switch(struct gps_device_t *session, speed_t speed, char parity, int stopbits);

static const char *str_message_id(uint16_t value)
{
    switch (value) {
#define CASE(x) case MSGID_##x: return #x;
        CASE(LOG        )
        CASE(UNLOGALL   )
        CASE(VERSION    )
        CASE(BESTPOS    )
        CASE(BESTXYZ    )
        CASE(TIME       )
        CASE(PPSCONTROL )
        CASE(GPGGA      )
        CASE(GPGSA      )
        CASE(GPGSV      )
        CASE(GPRMB      )
        CASE(GPRMC      )
        CASE(GPVTG      )
        CASE(GPGST      )
        CASE(GPZDA      )
#undef CASE
        default:
            return "unknown";
    }
}

static const char *str_time_status(enum GpsReferenceTimeStatus value)
{
    switch (value) {
#define CASE(x) case x: return #x;
        CASE(UNKNOWN           )
        CASE(APPROXIMATE       )
        CASE(COARSEADJUSTING   )
        CASE(COARSE            )
        CASE(COARSESTEERING    )
        CASE(FREEWHEELING      )
        CASE(FINEADJUSTING     )
        CASE(FINE              )
        CASE(FINEBACKUPSTEERING)
        CASE(FINESTEERING      )
        CASE(SATTIME           )
#undef CASE
        default: {
            static char buffer[64];
            memset(buffer, 0, sizeof(buffer));
            sprintf(buffer, "(invalid #%d)", (int)value);
            return buffer;
        }
    }
}

static const char *str_clock_status(enum ClockModelStatus value)
{
    switch (value) {
#define CASE(x) case CLOCK_STATUS__##x: return #x;
        CASE(VALID     )
        CASE(CONVERGING)
        CASE(ITERATING )
        CASE(INVALID   )
#undef CASE
        default: {
            static char buffer[64];
            sprintf(buffer, "(invalid #%d)", (int)value);
            return buffer;
        }
    }
}

static inline void putleu32(void *buf, off_t offset, uint32_t value)
{
    *(uint32_t *)((uint8_t *)buf + offset) = value;
}

static inline void put_crc(void *buf, size_t len)
{
    uint32_t crc = CalculateBlockCRC32(buf, len - sizeof(crc));
    putleu32(buf, len - sizeof(crc), crc);
}

static ssize_t novatel_crc_write(struct gps_device_t *session, void *buf, const size_t len)
{
    put_crc(buf, len);
    return gpsd_write(session, buf, len);
}

static ssize_t novatel_send(struct gps_device_t *session, uint16_t msg_id,
    const void *payload, size_t payload_length)
{
    const size_t header_length = 28;
    const size_t checksum_length = sizeof(uint32_t);
    const size_t buffer_length = header_length + payload_length + checksum_length;

    /* do not write if -b (readonly) option set */
    if (session->context->readonly)
        return true;

    char buffer[MAX_PACKET_LENGTH];
    char *header = (char *)&buffer[0];

    putbyte(header, 0, 0xAA);
    putbyte(header, 1, 0x44);
    putbyte(header, 2, 0x12);
    putbyte(header, 3, header_length); // Header Length
    putle16(header, 4, msg_id); // Message ID
    putbyte(header, 6, 0);    // Message Type: Binary | OriginalMsg
    putbyte(header, 7, 192);  // Port Address: THIS_PORT
    putle16(header, 8, payload_length); // Message Length
    putle16(header, 10, 0);   // Sequence (unused)
    memset(&header[12], 0, header_length - 12); // rest of header is zeros

    memcpy(&header[header_length], payload, payload_length); // copy payload into buffer

    return novatel_crc_write(session, buffer, buffer_length);
}

static void novatel_cmd_1(struct gps_device_t *session, uint16_t msg_id, trigger_t trigger, double period, double offset, bool hold)
{
    //int trigger = 2; // ONTIME
    //double period = 0.1; // Valid: 0.01, 0.02, 0.05, 0.1, 0.2, 0.25, 0.5, 1.0 and any integer value larger than 1.0
    //double offset = 0.0; // A valid value is any integer (whole number) smaller than the period.
    //bool hold = false; // 0=NOHOLD, 1=HOLD

    uint8_t data[32];
    putle32(data, 0, 192); // Output port: THISPORT
    putle16(data, 4, msg_id);
    putbyte(data, 6, 0); // Message type of log
    putbyte(data, 7, 0); // reserved
    putle32(data, 8, trigger); // binary log trigger (0=ONNEW, 1=ONCHANGED, 2=ONTIME, 3=ONNEXT, 4=ONCE, 5=ONMARK)
    putled64((char *)data, 12, period); // Log period (for ONTIME trigger) in seconds
    putled64((char *)data, 20, offset); // Offset for period (ONTIME trigger) in seconds
    putle32(data, 28, hold ? 1 : 0); // NOHOLD - Allow log to be removed by the UNLOGALL command

    (void) novatel_send(session, 1, data, sizeof(data)); // LOG
}

static inline void novatel_cmd_1_ontime(struct gps_device_t *session, uint16_t msg_id, double period, double offset)
{
    // TODO: verify input arguments are correct
    novatel_cmd_1(session, msg_id, ONTIME, period, offset, false);
}

static inline void novatel_cmd_1_cycle(struct gps_device_t *session, uint16_t msg_id)
{
    double period = session->gpsdata.dev.cycle.tv_sec +
                   (session->gpsdata.dev.cycle.tv_nsec / 1000000000.0);
    double offset = 0.0;
    novatel_cmd_1(session, msg_id, ONTIME, period, offset, false);
}

static inline void novatel_cmd_1_once(struct gps_device_t *session, uint16_t msg_id)
{
    novatel_cmd_1(session, msg_id, ONCE, 0.0, 0.0, false);
}

static void novatel_cmd_18__reset(struct gps_device_t *session, int delay)
{
    uint8_t data[4];
    putle32(data, 0, delay); // Seconds to wait before resetting (default=0)

    (void) novatel_send(session, 18, data, sizeof(data)); // RESET

    GPSD_LOG(LOG_PROG, &session->context->errout,
        "NovAtel: sent command #38 RESET (delay=%d)\n", delay);
}

static void novatel_cmd_38__unlogall(struct gps_device_t *session)
{
    uint8_t data[8];
    putle32(data, 0, 192); // Port to clear: THISPORT
    putle32(data, 4, 0);   // Does not remove logs with the HOLD parameter

    (void) novatel_send(session, 38, data, sizeof(data)); // UNLOGALL

    GPSD_LOG(LOG_PROG, &session->context->errout,
        "NovAtel: sent command #38 UNLOGALL (Removes all logs from logging control)\n");
}

static void novatel_cmd_613__ppscontrol(struct gps_device_t *session, int pps_switch, bool pps_polarity, double period, uint32_t pulsewidth)
{
    uint8_t data[20];
    putle32(data, 0, pps_switch); // 0 = DISABLE, 1 = ENABLE, 2 = ENABLE_FINETIME, 3 = ENABLE_FINETIME_MINUTEALIGN
    putle32(data, 4, pps_polarity); // 0 = NEGATIVE, 1 = POSITIVE
    putled64((void*)data, 8, period); // 0.05, 0.1, 0.2, 0.25, 0.5, 1.0, 2.0, 3.0, ..., 20.0
    putle32(data, 16, pulsewidth); // in microseconds (default=1000)

    (void) novatel_send(session, 613, data, sizeof(data)); // PPSCONTROL

    GPSD_LOG(LOG_PROG, &session->context->errout,
        "NovAtel: sent command #613 PPSCONTROL\n");
    GPSD_LOG(LOG_DATA, &session->context->errout,
        "NovAtel: PPS: %s %s %f %dus\n",
            pps_switch == 0 ? "disable" : "enable",
            pps_polarity ? "positive" : "negative",
            period,
            pulsewidth);
}

/* Configures serial port settings */
static void novatel_cmd_1246__serialconfig(struct gps_device_t *session, speed_t speed, int parity, int stopbits)
{
    //speed_t speed = 115200; // Selection: 2400, 4800, 9600, 19200, 38400, 57600, 115200, 230400 or 460800
    //int parity = 0; // Selection: 0=none, 1=even, 2=odd
    int databits = 8; // Selection: 7 or 8
    //int stopbits = 1; // Selection: 1 or 2
    int handshaking = 0; // Selection: 0=none, 1=xon, 2=cts
    int break_detection = 1; // Selection: 0=off or 1=on

    uint8_t data[28];
    putle32(data, 0, SERIAL__THIS_PORT); // Port to configure
    putle32(data, 4, speed);  // Communication baud rate (bps)
    putle32(data, 8, parity); // Parity (0=none, 1=even or 2=odd)
    putle32(data, 12, databits); // Number of data bits (7 or 8)
    putle32(data, 16, stopbits); // Number of stop bits (1 or 2)
    putle32(data, 20, handshaking); // Handshaking (0=none, 1=xon or 2=cts)
    putle32(data, 24, break_detection); // Break detection (0=OFF or 1=ON)

    (void) novatel_send(session, 1246, data, sizeof(data)); // SERIALCONFIG

    GPSD_LOG(LOG_PROG, &session->context->errout,
        "NovAtel: sent command #1246 SERIALCONFIG (Configure serial port settings)\n");
    GPSD_LOG(LOG_DATA, &session->context->errout,
        "NovAtel: settings: %d %s %d %d %s %s\n",
            speed,
            parity == 0 ? "none" : parity == 1 ? "even" : "odd",
            databits,
            stopbits,
            handshaking == 0 ? "none" : handshaking == 1 ? "xon" : "cts",
            break_detection == 0 ? "off" : "on");
}

static void novatel_cmd_1247__echo(struct gps_device_t *session, bool echo)
{
    uint8_t data[8];
    putle32(data, 0, SERIAL__THIS_PORT); // Port to configure
    putle32(data, 4, echo ? 1 : 0);

    (void) novatel_send(session, 1247, data, sizeof(data)); // ECHO

    GPSD_LOG(LOG_PROG, &session->context->errout,
        "NovAtel: sent command #1247 ECHO (Command is used to set a port to echo)\n");
    GPSD_LOG(LOG_DATA, &session->context->errout,
        "NovAtel: echo %s\n", echo ? "ON" : "OFF");
}

static gps_mask_t novatel_parse_time(struct gps_device_t * session, unsigned char *buf, size_t len UNUSED)
{
    gps_mask_t mask = 0;

    const size_t hdrlen = getub(buf, OFFSET_HEADER_LENGTH);
    const size_t msglen = getleu16(buf, OFFSET_MESSAGE_LENGTH);

    const char *header = (void *)buf;
    const char *data = (void *)&buf[hdrlen];

    /* check that message length is valid */
    if (msglen != 44) {
        GPSD_LOG(LOG_WARN, &session->context->errout,
                 "NovAtel: TIME bad message length %zu\n", msglen);
        return 0;
    }

    GPSD_LOG(LOG_PROG, &session->context->errout,
             "NovAtel: TIME - time data\n");

    /* Timestamp */
    uint16_t week = getleu16(header, OFFSET_GPS_WEEK);
    uint32_t tow = getleu32(header, OFFSET_GPS_TOW); /* tow in ms */
    timespec_t ts_tow; MSTOTS(&ts_tow, tow);
    session->newdata.time = gpsd_gpstime_resolv(session, week, ts_tow);
    mask |= TIME_SET;

    enum ClockModelStatus clock_status = getleu32(data, 0);
    double offset = getled64(data, 4);
    double offset_std = getled64(data, 12);
    double utc_offset = getled64(data, 20);
    int utc_year = getleu32(data, 28);
    int utc_month = getub(data, 32);
    int utc_day = getub(data, 33);
    int utc_hour = getub(data, 34);
    int utc_min = getub(data, 35);
    int utc_ms = getleu32(data, 36);
    int utc_status = getleu32(data, 40);

    char ts_buf[TIMESPEC_LEN];
    GPSD_LOG(LOG_DATA, &session->context->errout,
             "TIME: time=%s utc=%04d-%02d-%02d %02d:%02d:%02d.%03d status=%s utc-offset=%f gps-offset=%f (%f) clock=%s\n",
             timespec_str(&session->newdata.time, ts_buf, sizeof(ts_buf)),
             utc_year, utc_month, utc_day,
             utc_hour, utc_min, utc_ms/1000, utc_ms%1000,
             utc_status == 0 ? "invalid" : utc_status == 1 ? "valid" : utc_status == 2 ? "warning" : "(unknown)",
             utc_offset,
             offset, offset_std,
             str_clock_status(clock_status));

    return mask;
}

static gps_mask_t novatel_parse_bestpos(struct gps_device_t * session, unsigned char *buf, size_t len UNUSED)
{
    gps_mask_t mask = 0;

    const size_t hdrlen = getub(buf, OFFSET_HEADER_LENGTH);
    const size_t msglen = getleu16(buf, OFFSET_MESSAGE_LENGTH);

    const char *header = (void *)buf;
    const char *data = (void *)&buf[hdrlen];

    /* check that message length is valid */
    if (msglen != 72) {
        GPSD_LOG(LOG_WARN, &session->context->errout,
                 "NovAtel: BESTPOS bad message length %zu\n", msglen);
        return 0;
    }

    GPSD_LOG(LOG_PROG, &session->context->errout,
             "NovAtel: BESTPOS - navigation data\n");

    /* Timestamp */
    uint16_t week = getleu16(header, OFFSET_GPS_WEEK);
    uint32_t tow = getleu32(header, OFFSET_GPS_TOW); /* tow in ms */
    timespec_t ts_tow; MSTOTS(&ts_tow, tow);
    session->newdata.time = gpsd_gpstime_resolv(session, week, ts_tow);
    mask |= TIME_SET;

    enum SolutionStatus solution_status = getub(data, 0);
    uint32_t pos_type = getub(data, 4);
    double lat = getled64(data, 8);             // lat      Latitude (degrees)
    double lon = getled64(data, 16);            // lon      Longitude (degrees)
    double hgt = getled64(data, 16);            // hgt      Height above mean sea level (metres)

    session->newdata.latitude = lat;
    session->newdata.longitude = lon;
    session->newdata.altMSL = hgt;
    mask |= LATLON_SET | ALTITUDE_SET;

    /* TODO: fixme */
    (void)solution_status;
    (void)pos_type;
    session->newdata.mode = MODE_NO_FIX; // FIXME: use data in packet to build mode
    session->newdata.status = STATUS_UNK; // FIXME: use data in packet build status
    mask |= MODE_SET | STATUS_SET;

    char ts_buf[TIMESPEC_LEN];
    GPSD_LOG(LOG_DATA, &session->context->errout,
             "BESPOS: time=%s lat=%.2f lon=%.2f altMSL %.2f mode=%d status=%d\n",
             timespec_str(&session->newdata.time, ts_buf, sizeof(ts_buf)),
             session->newdata.latitude,
             session->newdata.longitude,
             session->newdata.altMSL,
             session->newdata.mode,
             session->newdata.status);

    return mask;
}

static gps_mask_t novatel_parse_bestxyz(struct gps_device_t * session, unsigned char *buf, size_t len UNUSED)
{
    // Note: https://gssc.esa.int/navipedia/index.php/Positioning_Error

    double px, py, pz;                  /* Cartesian coordinates [m] */
    float px_sigma, py_sigma, pz_sigma; /* Position spherical error probability (SEP) [m] */
    double vx, vy, vz;                  /* Cartesian velocities [m/s] */
    float vx_sigma, vy_sigma, vz_sigma; /* Velocity SEP [m/s] */
    enum SolutionStatus p_solution_status, v_solution_status;
    enum PosVelType p_type, v_type;
    gps_mask_t mask = 0;

    const size_t hdrlen = getub(buf, OFFSET_HEADER_LENGTH);
    const size_t msglen = getleu16(buf, OFFSET_MESSAGE_LENGTH);

    const char *header = (void *)buf;
    const char *data = (void *)&buf[hdrlen];

    /* check that message length is valid */
    if (msglen != 112) {
        GPSD_LOG(LOG_WARN, &session->context->errout,
                 "NovAtel: BESTXYZ bad message length %zu\n", msglen);
        return 0;
    }

    GPSD_LOG(LOG_PROG, &session->context->errout,
             "NovAtel: BESTXYZ - navigation data\n");

    /* Timestamp */
    uint16_t week = getleu16(header, OFFSET_GPS_WEEK);
    uint32_t tow = getleu32(header, OFFSET_GPS_TOW); /* tow in ms */
    timespec_t ts_tow; MSTOTS(&ts_tow, tow);
    session->newdata.time = gpsd_gpstime_resolv(session, week, ts_tow);
    enum GpsReferenceTimeStatus time_status = (int)getub(header, OFFSET_TIME_STATUS);
    GPSD_LOG(LOG_PROG, &session->context->errout,
             "NovAtel: BESTXYZ - week=%u, tow=%u, time_status=%u %s\n",
             week, tow, time_status, str_time_status(time_status));
    mask |= TIME_SET;

    p_solution_status = getub(data, 0);
    p_type = getub(data, 4);
    px = getled64(data, 8);             // P-X      Position X-coordinate (m)
    py = getled64(data, 16);            // P-Y      Position Y-coordinate (m)
    pz = getled64(data, 24);            // P-Z      Position Z-coordinate (m)
    px_sigma = getlef32(data, 32);      // P-X σ    Standard deviation of P-X (m)
    py_sigma = getlef32(data, 36);      // P-Y σ    Standard deviation of P-Y (m)
    pz_sigma = getlef32(data, 40);      // P-Z σ    Standard deviation of P-Z (m)

    v_solution_status = getub(data, 44);
    v_type = getub(data, 48);
    vx = getled64(data, 52);            // V-X      Velocity vector along X-axis (m/s)
    vy = getled64(data, 60);            // V-Y      Velocity vector along Y-axis (m/s)
    vz = getled64(data, 68);            // V-Z      Velocity vector along Z-axis (m/s)
    vx_sigma = getlef32(data, 76);      // V-X σ    Standard deviation of V-X (m/s)
    vy_sigma = getlef32(data, 80);      // V-Y σ    Standard deviation of V-Y (m/s)
    vz_sigma = getlef32(data, 84);      // V-Z σ    Standard deviation of V-Z (m/s)

    session->newdata.ecef.x = px;
    session->newdata.ecef.y = py;
    session->newdata.ecef.z = pz;
    session->newdata.ecef.pAcc = sqrt(px_sigma*px_sigma + py_sigma*py_sigma + pz_sigma*pz_sigma);
    mask |= ECEF_SET;

    session->newdata.ecef.vx = vx;
    session->newdata.ecef.vy = vy;
    session->newdata.ecef.vz = vz;
    session->newdata.ecef.vAcc = sqrt(vx_sigma*vx_sigma + vy_sigma*vy_sigma + vz_sigma*vz_sigma);
    mask |= VECEF_SET;

    //switch (p_solution_status) {
    //case 5:
    //    session->newdata.mode = MODE_2D;
    //    break;
    //case 7:
    //    session->newdata.mode = MODE_3D;
    //    break;
    //default:
    //    session->newdata.mode = MODE_NO_FIX;
    //    break;
    //}
    //mask |= MODE_SET;

    (void)p_type;
    (void)p_solution_status;
    (void)v_type;
    (void)v_solution_status;
    // TODO: use p/v solution_status and p/v type to deterime these two fields
    //session->newdata.mode = MODE_3D; // MODE_2D // MODE_NO_FIX // MODE_NOT_SEEN
    //session->newdata.status = STATUS_FIX; // STATUS_NO_FIX
    //mask |= MODE_SET | STATUS_SET;

//#define STATUS_NO_FIX   0       // Unknown status, maybe no fix.
///* yes, plain GPS (SPS Mode), without DGPS, PPS, RTK, DR, etc. */
//#define STATUS_FIX      1
//#define STATUS_DGPS_FIX 2       /* yes, with DGPS */
//#define STATUS_RTK_FIX  3       /* yes, with RTK Fixed */
//#define STATUS_RTK_FLT  4       /* yes, with RTK Float */
//#define STATUS_DR       5       /* yes, with dead reckoning */
//#define STATUS_GNSSDR   6       /* yes, with GNSS + dead reckoning */
//#define STATUS_TIME     7       /* yes, time only (surveyed in, manual) */
//// Note that STATUS_SIM and MODE_NO_FIX can go together.
//#define STATUS_SIM      8       /* yes, simulated */
///* yes, Precise Positioning Service (PPS)
// * Not to be confused with Pulse per Second (PPS)
// * PPS is the encrypted military P(Y)-code */
//#define STATUS_PPS_FIX  9

    /*
     * At the end of each packet-cracking function, report at LOG_DATA level
     * the fields it potentially set and the transfer mask. Doing this
     * makes it relatively easy to track down data-management problems.
     */
    //GPSD_LOG(LOG_DATA, &session->context->errout,
    //         "OEM7: GPS Week: %u, GPS Time of Week: %u (GPS Time: %f)\n",
    //         week, tow, gps_time);
    char ts_buf[TIMESPEC_LEN];
    GPSD_LOG(LOG_DATA, &session->context->errout,
             "BESTXYZ: time=%s ecef p[x:%.2f y:%.2f z:%.2f] v[x:%.2f y:%.2f z:%.2f] "
             "mode=%d status=%d\n",
             timespec_str(&session->newdata.time, ts_buf, sizeof(ts_buf)),
             session->newdata.ecef.x,
             session->newdata.ecef.y,
             session->newdata.ecef.z,
             session->newdata.ecef.vx,
             session->newdata.ecef.vy,
             session->newdata.ecef.vz,
             session->newdata.mode,
             session->newdata.status);

    return mask;
}

static gps_mask_t novatel_parse_version(struct gps_device_t * session, unsigned char *buf, size_t len)
{
    struct component_info {
        uint32_t type;
        char model[16]; // OEM7 firmware model number
        char psn[16]; // Product Serial Number
        char hw_version[16]; // Hardware version (format: P-R) (e.g. OEM7700-1.00)
        char sw_version[16];
        char boot_version[16];
        char comp_date[12];
        char comp_time[12];
    };

    const char * const header = (void *)buf;
    gps_mask_t mask = 0;

    const char *data = (void *)&header[header_length];
    const size_t data_length = getleu16(header, 8);
    const size_t data_length_calculated = len - header_length - checksum_length;

    if (data_length != data_length_calculated) {
        GPSD_LOG(LOG_WARN, &session->context->errout,
                 "NovAtel: VERSION bad message len %zu\n", data_length);
        return 0;
    }

    const char *component = &data[4];
    const size_t component_length = 108;

    unsigned int number_of_components = getleu32(data, 0);
    unsigned int number_of_components_calculated = (data_length - 4) / component_length;

    if (number_of_components != number_of_components_calculated || ((data_length - 4) % component_length) != 0) {
        GPSD_LOG(LOG_WARN, &session->context->errout,
                 "NovAtel: VERSION bad component count %zu\n", len);
        return 0;
    }

    if (number_of_components > 0) {
        /* Copy model numbers into subtype buffer (seperated by ';') */
        char *ptr = session->subtype;
        char *end = &session->subtype[sizeof(session->subtype) - 1];
        for (unsigned int i = 0; i < number_of_components; ++i) {
            const struct component_info *c = (void *)&component[i*component_length];

            size_t model_length = strnlen(c->model, sizeof(c->model));

            /* check for empty model strings and ignore them */
            if (model_length <= 0)
                continue;

            /* add seperator */
            if (i && ptr < end)
                *ptr++ = ';';

            /* add model number string */
            size_t remaining = (size_t)(ptr - end);
            if (remaining < model_length) {
                memcpy(ptr, c->model, remaining);
                ptr += remaining;
                break; /* no more space in buffer, skip rest of components */
            } else {
                memcpy(ptr, c->model, model_length);
                ptr += model_length;
            }
        }
        *ptr = '\0'; /* ensure string is null-terminated */
    }

    mask |= DEVICEID_SET;

    GPSD_LOG(LOG_PROG, &session->context->errout,
             "NovAtel: VERSION - version information for %d component%s\n",
             number_of_components, number_of_components != 1 ? "s" : "");
    GPSD_LOG(LOG_DATA, &session->context->errout,
             "NovAtel: subtype = '%s'\n", session->subtype);

    return mask;
}

#include <time.h>
//#include <sys/time.h>
//#include <linux/time.h>

static gps_mask_t novatel_parse_binary(struct gps_device_t * session, unsigned char *buf, size_t len)
{
    /* make sure there is enough bytes read some header fields */
    if (len < 10)
        return 0;

#if 1 // FOR TESTING
    {
        FILE *f = fopen("/tmp/gpsd-novatel.log", "a");
        if (f) {
            struct timespec tm; clock_gettime(CLOCK_MONOTONIC, &tm); fprintf(f, "%ld.%06ld: ", tm.tv_sec, tm.tv_nsec/1000);
            for (size_t i = 0; i < len; ++i) { fprintf(f, "%02x", buf[i]); } fprintf(f, "\n"); // ascii
            //fwrite(buf, 1, len, f); // binary
            fclose(f);
        }
    }
#endif

    /* read some header fields to check lengths and find id/type of message */
    uint8_t hdrlen = getub(buf, 3);
    uint16_t msgid = getleu16(buf, 4);
    uint8_t msgtype = getub(buf, 6);
    uint16_t msglen = getleu16(buf, 8);

    /* verify the length fields of the message, the CRC has already been checked */
    if (len != hdrlen + msglen + checksum_length) {
        GPSD_LOG(LOG_WARN, &session->context->errout,
                 "NovAtel: bad len %zu\n", len);
        return 0;
    }

    /* check for a response message */
    if ((msgtype & 0x80) != 0) {
        unsigned char *data = &buf[hdrlen];
        int responseId = getleu32(data, 0);
        const char *responseMsg = (void *)&data[4];
        buf[len - checksum_length] = '\0'; /* ensure null-terminated string */

        GPSD_LOG(LOG_DATA, &session->context->errout,
                 "NovAtel: response #%d: %s (id: %d %s)\n",
                 responseId, responseMsg, msgid, str_message_id(msgid));

        /* TODO: Now what? What should we do with the response message */

        return 0;
    }

    GPSD_LOG(LOG_DATA, &session->context->errout,
             "NovAtel: message id=%u %s, type=0x%02x, length=%zu bytes\n",
             msgid, str_message_id(msgid), msgtype, len);

    switch (msgid) {
    case MSGID_BESTPOS:
        return novatel_parse_bestpos(session, buf, len);
    case MSGID_BESTXYZ:
        return novatel_parse_bestxyz(session, buf, len);
    case MSGID_TIME:
        return novatel_parse_time(session, buf, len);
    case MSGID_VERSION:
        return novatel_parse_version(session, buf, len);
    default:
        /* FIX-ME: This gets noisy in a hurry. Change once your driver works */
        GPSD_LOG(LOG_WARN, &session->context->errout,
                "NovAtel: unknown message (id=%u, type=0x%02x, length %zd bytes)\n",
                msgid, msgtype, len);
        return 0;
    }

    return 0;
}

static gps_mask_t novatel_parse_input(struct gps_device_t *session)
{
    if (session->lexer.type ==  NOVATEL_OEM7_PACKET) {
        return novatel_parse_binary(session, session->lexer.outbuffer,
                                             session->lexer.outbuflen);
#ifdef NMEA0183_ENABLE
    } else if (session->lexer.type == NMEA_PACKET) {
        return nmea_parse((char *)session->lexer.outbuffer, session);
#endif /* NMEA0183_ENABLE */
    } else {
        return 0;
    }
}

static bool novatel_speed_switch(struct gps_device_t *session, speed_t speed, char parity, int stopbits)
{
    GPSD_LOG(LOG_PROG, &session->context->errout,
             "NovAtel: Switch serial port to %d %c %d.\n",
             speed, parity, stopbits);

    switch (speed) {
    case 2400:
    case 4800:
    case 9600:
    case 19200:
    case 38400:
    case 57600:
    case 115200:
    case 230400:
    case 460800:
        break;
    default:
        return false; // invalid parameter value
    }

    switch (parity) {
    case 'n':
    case 'N':
    case 0:
        parity = SERIAL__PARITY_NONE;
        break;
    case 'o':
    case 'O':
    case 1:
        parity = SERIAL__PARITY_ODD;
        break;
    case 'e':
    case 'E':
    case 2:
        parity = SERIAL__PARITY_EVEN;
        break;
    default:
        return false; // invalid parameter value
    }

    switch (stopbits) {
    case 1:
    case 2:
        break;
    default:
        return false; // invalid parameter value
    }

    novatel_cmd_1246__serialconfig(session, speed, parity, stopbits);

    return true; /* it would be nice to error-check this */
}

static const char *strevent(event_t event)
{
    switch(event) {
#define CASE(x) case x: return #x
    CASE(event_wakeup);
    CASE(event_triggermatch);
    CASE(event_identified);
    CASE(event_configure);
    CASE(event_driver_switch);
    CASE(event_deactivate);
    CASE(event_reactivate);
#undef CASE
    default:
        return "(unknown)";
    }
}

static void novatel_event_hook(struct gps_device_t *session, event_t event)
{
    if (session->context->readonly ||
        session->context->passive)
        return;

    GPSD_LOG(LOG_PROG, &session->context->errout,
             "NovAtel: ============================================================\n");
    GPSD_LOG(LOG_PROG, &session->context->errout,
             "NovAtel: Called event_hook function: %d %s\n", (int)event, strevent(event));
    GPSD_LOG(LOG_PROG, &session->context->errout,
             "NovAtel: ============================================================\n");

    if (event == event_wakeup) {
        /*
         * Code to make the device ready to communicate.  Only needed if the
         * device is in some kind of sleeping state, and only shipped to
         * RS232C, so that gpsd won't send strings to unidentified USB devices
         * that might not be GPSes at all.
         */
    }
    else if (event == event_deactivate) {
        /*
         * Fires when the device is deactivated.  Use this to revert
         * whatever was done at event_identified and event_configure
         * time.
         */
        novatel_cmd_38__unlogall(session);
        novatel_speed_switch(session, 9600, 'N', 1);
    }
    else if (event == event_reactivate) {
       /*
        * Fires when a device is reactivated after having been closed.
        * Use this hook for re-establishing device settings that
        * it doesn't hold through closes.
        */

        /* reset binary initialization steps */
        session->cfg_stage = 3;
        session->cfg_step = 0;

        novatel_cmd_38__unlogall(session);
    }
    else if (event == event_identified) {
        /*
         * Fires when the first full packet is recognized from a
         * previously unidentified device.  The session.lexer counter
         * is zeroed.  If your device has a default cycle time other
         * than 1 second, set session->device->gpsdata.cycle here. If
         * possible, get the software version and store it in
         * session->subtype.
         */
        return;
    }
    else if (event == event_configure) {

        if (session->cfg_stage == UINT_MAX) {
            /* configuration is done, nothing more to do here */
            return;
        }

        uint8_t msg_type = session->lexer.outbuffer[6]; // Message Type: bit 7 = Response bit

        session->cfg_step++;

        GPSD_LOG(LOG_PROG, &session->context->errout,
            "NovAtel: event_configure (lexer.counter = %d, config(stage=%d, step=%d), msg type = %s) -------------------------\n",
            session->lexer.counter,
            session->cfg_stage, session->cfg_step,
            msg_type & BIT(7) ? "Response" : "Original");

        if ((msg_type & BIT(7)) == 0 && session->cfg_step < 12) {
            /* we are waiting for a response message, ignore others */
            GPSD_LOG(LOG_PROG, &session->context->errout,
                "NovAtel: skip original message\n");
            return;
        }

        session->cfg_step = 0;
        session->cfg_stage++;

        //uint32_t response_id = getleu32(session->lexer.outbuffer, header_length);

        switch (session->cfg_stage) {
            case 0:
                break;

            case 1:
                novatel_speed_switch(session, 460800, 'N', 1);
                //novatel_speed_switch(session, 115200, 'N', 1);
                //novatel_speed_switch(session,   9600, 'N', 1);
                break;

            case 2:
                gpsd_set_speed(session, 460800, 'N', 1);
                //gpsd_set_speed(session, 115200, 'N', 1);
                //gpsd_set_speed(session, 9600, 'N', 1);
#if 1
                session->gpsdata.dev.cycle.tv_sec = 1;
                session->gpsdata.dev.cycle.tv_nsec = 0L;
#else
                session->gpsdata.dev.cycle.tv_sec = 0;
                session->gpsdata.dev.cycle.tv_nsec = 100000000L;
#endif
                /* query for version information */
                novatel_cmd_1_once(session, MSGID_VERSION);
                break;

            case 3:
                GPSD_LOG(LOG_PROG, &session->context->errout,
                     "NovAtel: Requesting NMEA logs.\n");
                novatel_cmd_1_cycle(session, MSGID_GPGGA); // NMEA: GPS fix data and undulation
                break;
            case 4:
                novatel_cmd_1_cycle(session, MSGID_GPGSV); // NMEA: GPS satellites in view
                break;
            case 5:
                novatel_cmd_1_cycle(session, MSGID_GPGSA); // NMEA: GPS DOP and active satellites
                break;
            case 6:
                novatel_cmd_1_cycle(session, MSGID_GPRMC); // NMEA: GPS specific information
                break;
            case 7:
                novatel_cmd_1_cycle(session, MSGID_GPVTG); // NMEA: Track made good and ground speed
                break;
            case 8:
                novatel_cmd_1_cycle(session, MSGID_GPGST); // NMEA: Estimated error in position solution
                break;
            case 9:
                novatel_cmd_1_cycle(session, MSGID_GPZDA); // NMEA: UTC time and date
                break;
            case 10:
                GPSD_LOG(LOG_PROG, &session->context->errout,
                     "NovAtel: Requesting periodic ECEF logs. (BESTXYZ)\n");
                novatel_cmd_1_cycle(session, MSGID_BESTXYZ);  // ECEF coordinates
                break;
            case 11:
                GPSD_LOG(LOG_PROG, &session->context->errout,
                     "NovAtel: Requesting periodic position logs. (BESTPOS)\n");
                novatel_cmd_1_cycle(session, MSGID_BESTPOS);
                break;
            case 12:
                GPSD_LOG(LOG_PROG, &session->context->errout,
                     "NovAtel: Requesting periodic time logs. (TIME)\n");
                novatel_cmd_1_ontime(session, MSGID_TIME, 1.0, 0.0); // fixed at 1Hz
                break;
            default:
                /* initialization is done */
                session->cfg_stage = UINT_MAX;
                session->cfg_step = 0;
                break;
        }
    }
    else if (event == event_driver_switch) {
        /*
         * Fires when the driver on a device is changed *after* it
         * has been identified.
         */

        /* forces a reconfigure as the following packets come in */
        session->lexer.counter = 0;
    }
}

static ssize_t novatel_control_send(struct gps_device_t *session, char *buf, size_t len)
{
    char *header = &session->msgbuf[0];
    const size_t header_length = 28;
    char *payload = &header[header_length];
    const size_t payload_length = len;
    //char *checksum = &payload[payload_length];
    const size_t checksum_length = sizeof(uint32_t);

    putbyte(header, 0, 0xAA);
    putbyte(header, 1, 0x44);
    putbyte(header, 2, 0x12);
    putbyte(header, 3, header_length);
    // TODO: fill in the header here

    memcpy(payload, buf, payload_length);

    //uint32_t crc = CalculateBlockCRC32((unsigned char *)payload, payload_length);
    //putle32(checksum, 0, crc);

    //session->msgbuflen = (size_t)(&checksum[4] - header);
    //put_crc(session->msgbuf, session->msgbuflen)
    //return gpsd_write(session, session->msgbuf, session->msgbuflen);
    session->msgbuflen = header_length + payload_length + checksum_length;
    return novatel_crc_write(session, session->msgbuf, session->msgbuflen);
}

static void novatel_init_query(struct gps_device_t *session)
{
    GPSD_LOG(LOG_PROG, &session->context->errout,
             "\n\nNovAtel: Probing for firmware version.\n");

    /* reset binary init steps */
    session->cfg_stage = 0;
    session->cfg_step = 0;

    /* query for version information */
    (void) novatel_cmd_1_once(session, MSGID_VERSION);
}

static void novatel_mode_switch(struct gps_device_t *session, int mode)
{
    GPSD_LOG(LOG_PROG, &session->context->errout,
             "NovAtel: Switch mode to %d %s.\n",
             mode,
             mode == MODE_NMEA ? "NMEA" :
             mode == MODE_BINARY ? "binary" : "unknown");

    switch (mode) {
        case MODE_NMEA:
            novatel_cmd_38__unlogall(session);
            novatel_cmd_1_ontime(session, MSGID_GPGGA, 1.0, 0.0); // NMEA: GPS fix data and undulation
            novatel_cmd_1_ontime(session, MSGID_GPGSA, 1.0, 0.0); // NMEA: GPS DOP and active satellites
            novatel_cmd_1_ontime(session, MSGID_GPGSV, 1.0, 0.0); // NMEA: GPS satellites in view
            novatel_cmd_1_ontime(session, MSGID_GPRMB, 1.0, 0.0); // NMEA: Navigation information
            novatel_cmd_1_ontime(session, MSGID_GPRMC, 1.0, 0.0); // NMEA: GPS specific information
            novatel_cmd_1_ontime(session, MSGID_GPVTG, 1.0, 0.0); // NMEA: Track made good and ground speed
            novatel_cmd_1_ontime(session, MSGID_GPGST, 1.0, 0.0); // NMEA: Estimated error in position solution
            novatel_cmd_1_ontime(session, MSGID_GPZDA, 1.0, 0.0); // NMEA: UTC time and date
            break;
        case MODE_BINARY:
            // Hmm, how to switch to Binary mode?
            novatel_cmd_38__unlogall(session);
            break;
    }
}

// 'gpsd' parses the following NMEA sentences: RMC, GGA, GLL, GSA, GSV, VTG, ZDA, GBS, HDT, DBT, GST.

//static bool novatel_rate_switcher(struct gps_device_t *session, double rate)
//{
//    // TODO: not implemented yet
//}

static bool novatel_probe_detect(struct gps_device_t *session)
{
    /* At start-up, the port speed will be 9600 bps */
    speed_t old_baudrate = session->gpsdata.dev.baudrate;
    char old_parity = session->gpsdata.dev.parity;
    unsigned int old_stopbits = session->gpsdata.dev.stopbits;
    gpsd_set_speed(session, 9600, 'N', 1);

    novatel_cmd_1247__echo(session, false); /* ECHO OFF */

    //session->lexer.errout = session->context->errout;

    for (int n = 0; n < 9; n++) {
        /* wait one second */
        struct timespec to = { .tv_sec = 1 };
        if (!nanowait(session->gpsdata.gps_fd, &to))
            break;
        if (generic_get(session) >= 0) {
            if (session->lexer.type == NOVATEL_OEM7_PACKET) {

                uint8_t *data = &session->lexer.outbuffer[28];
                int responseId = getleu32(data, 0);
                const char *responseMsg = (void *)&data[4];
                data[session->lexer.outbuflen - 28 - 4] = '\0'; // ensure null-terminated string

                GPSD_LOG(LOG_RAW, &session->context->errout,
                         "NovAtel: probe_detect found packet (response #%d %s)\n",
                         responseId, responseMsg);
                return true;
            }
        }
    }

    /* return serial port to original settings */
    gpsd_set_speed(session, old_baudrate, old_parity, old_stopbits);

    return false;
}

/* *INDENT-OFF* */
const struct gps_type_t driver_novatel = {
    /* Full name of type */
    .type_name          = "NovAtel",
    /* Associated lexer packet type */
    .packet_type        = NOVATEL_OEM7_PACKET,
    /* Driver type flags */
    .flags               = DRIVER_STICKY,
    /* Response string that identifies device (not active) */
    .trigger            = NULL,
    /* Number of satellite channels supported by the device */
    .channels           = 12,
    /* Startup-time device detector */
    .probe_detect       = novatel_probe_detect,
    /* Packet getter (using default routine) */
    .get_packet         = generic_get,
    /* Parse message packets */
    .parse_packet       = novatel_parse_input,
    /* RTCM handler (using default routine) */
    .rtcm_writer        = gpsd_write,
    /* non-perturbing initial query */
    .init_query         = novatel_init_query,
    /* Fire on various lifetime events */
    .event_hook         = novatel_event_hook,
    /* Speed (baudrate) switch */
    .speed_switcher     = novatel_speed_switch,
    /* Switch to NMEA mode */
    .mode_switcher      = novatel_mode_switch,
    /* Message delivery rate switcher */
    .rate_switcher      = NULL, // novatel_rate_switcher,
    /* Minimum cycle time */
    .min_cycle.tv_sec  = 1,
    .min_cycle.tv_nsec = 0,
    //.min_cycle.tv_sec  = 0, // Valid: 0.01, 0.02, 0.05, 0.1, 0.2, 0.25, 0.5, 1.0 and above
    //.min_cycle.tv_nsec = 10*000*000,
    /* Control string sender - should provide checksum and trailer */
    .control_send       = novatel_control_send, // how to send a control string
    .time_offset     = NULL,            /* no method for NTP fudge factor */
};
/* *INDENT-ON* */

#endif /* defined(NOVATEL_ENABLE) && defined(BINARY_ENABLE) */

// vim: set expandtab shiftwidth=4