#include "../include/gpsd_config.h"  /* must be before all includes */

#include "../include/gpsd.h"
#include "../include/bits.h"
#include "../include/gpsmon.h"

#ifdef NOVATEL_ENABLE
//#include "../include/driver_novatel.h"
extern const struct gps_type_t driver_novatel;

static bool novatel_initialize(void)
{

    return true; /* success */
}

static void novatel_update(void)
{
    // Called on each packet received.
}

static int novatel_command(char line[])
{
    (void)line;
    return COMMAND_UNKNOWN;
}

static void novatel_wrap(void)
{
}


const struct monitor_object_t novatel_mmt = {
    .initialize = novatel_initialize,
    .update = novatel_update,
    .command = novatel_command,
    .wrap = novatel_wrap,
    .min_y = 23, .min_x = 80, /* size of the device window */
    .driver = &driver_novatel,
};

#endif /* NOVATEL_ENABLE */
// vim: set expandtab shiftwidth=4
