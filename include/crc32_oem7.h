/* Interface for CRC-32 cyclic redundancy checksum code
 *
 * This file is Copyright 2021 by the GPSD project
 * SPDX-License-Identifier: BSD-2-clause
 */
#ifndef __CRC32_OEM7_H__
#define __CRC32_OEM7_H__

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

extern uint32_t CalculateBlockCRC32(const void *buffer, size_t count);

static inline bool CheckBlockCRC32(const void *buffer, size_t count)
{
    return 0 == CalculateBlockCRC32(buffer, count);
}

#endif /* __CRC32_OEM7_H__ */
