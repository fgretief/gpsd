#ifndef _GPSD_NOVATEL_H_
#define _GPSD_NOVATEL_H_

/*
 * NovAtel OEM7 Commands and Logs Reference Manual
 *
 * Revision Level: v18
 * Revision Date: May 2021
 * Publication Number: OM-20000169
 * Firmware Versions:
 *   7.08.01 / OM7MR0801RN0000
 *   7.08.01 / OA7CR0801RN0000
 *   PP7 7.08 / EP7PR0800RN0000
 *   CPT7 7.08.02 / SC7PR0802RN0000
 */

#define MSGID_LOG           (1)
#define MSGID_UNLOGALL      (38)

/* Version information for all components of a system */
#define MSGID_VERSION       (37)   // Paragraph 3.214 on page 960

/* Best available ?  */
#define MSGID_BESTPOS       (42)   // Paragraph 3.17 on page 497

/* Best available cartesian position and velocity */
#define MSGID_BESTXYZ       (241)  // Paragraph 3.21 on page 512

#define MSGID_TIME          (101)
#define MSGID_PPSCONTROL    (613)

#define MSGID_GPGGA         (218)
#define MSGID_GPGSA         (221)
#define MSGID_GPGSV         (223)
#define MSGID_GPRMB         (224)
#define MSGID_GPRMC         (225)
#define MSGID_GPVTG         (226)
#define MSGID_GPGST         (222)
#define MSGID_GPZDA         (227)

/* Table 12 on page 52 */
enum GpsReferenceTimeStatus {
    UNKNOWN                 = 20,   // Time validity is unknown
    APPROXIMATE             = 60,   // Time is set approximately
    COARSEADJUSTING         = 80,   // Time is approaching coarse precision
    COARSE                  = 100,  // This time is valid to coarse precision
    COARSESTEERING          = 120,  // Time is coarse set and is being steered
    FREEWHEELING            = 130,  // Position is lost and the range bias cannot be calculated
    FINEADJUSTING           = 140,  // Time is adjusting to fine precision
    FINE                    = 160,  // Time has fine precision
    FINEBACKUPSTEERING      = 170,  // Time is fine set and is being steered by the backup system
    FINESTEERING            = 180,  // Time is fine set and is being steered
    SATTIME                 = 200,  // Time from satellite. Only used in logs containing satellite data such as ephemeris and almanac
};

/* Table 57: "Binary Log Triggers" on page 247 */
typedef enum {
    ONNEW                   = 0,    // Does not output current message but outputs when the message is updated (not necessarily changed)
    ONCHANGED               = 1,    // Outputs the current message and then continues to output when the message is changed
    ONTIME                  = 2,    // Output on a time interval
    ONNEXT                  = 3,    // Output only the next message
    ONCE                    = 4,    // Output only the current message (default). If no message is currently present, the next message is output when available.
    ONMARK                  = 5,    // Output when a pulse is detected on the mark 1 input, MK1I
} trigger_t;

/* Table 73 on page 375 */
enum {
    SERIAL__COM1            = 1,
    SERIAL__COM2            = 2,
    SERIAL__COM3            = 3,
    SERIAL__THIS_PORT       = 6,
    SERIAL__COM4            = 19,
    SERIAL__IMU             = 21,
    SERIAL__COM5            = 31,
    SERIAL__COM6            = 32,
    SERIAL__BT1             = 33,
    SERIAL__COM7            = 34,
    SERIAL__COM8            = 35,
    SERIAL__COM9            = 36,
    SERIAL__COM10           = 37,
};

/* Table 74 on page 367 */
enum {
    SERIAL__PARITY_NONE     = 0,
    SERIAL__PARITY_EVEN     = 1,
    SERIAL__PARITY_ODD      = 2
};

/* Table 75 on page 367 */
enum {
    SERIAL__HANDSHAKING_NONE = 0,   // No handshaking (default)
    SERIAL__HANDSHAKING_XON = 1,    // XON/XOFF software handshaking
    SERIAL__HANDSHAKING_CTS = 2,    // CTS/RTS hardware handshaking
};

/* Table 90 on page 500 */
enum SolutionStatus {
    SOL_COMPUTED            = 0,    // Solution computed
    INSUFFICIENT_OBS        = 1,    // Insufficient observations
    NO_CONVERGENCE          = 2,    // No convergence
    SINGULARITY             = 3,    // Singularity at parameters matrix
    CONV_TRACE              = 4,    // Covariance trace exceeds maximum (trace > 1000 m)
    TEST_DIST               = 5,    // Test distance exceeded (maximum of 3 rejections if distance >10 km)
    COLD_START              = 6,    // Not yet converged from cold start
    V_H_LIMIT               = 7,    // Height or velocity limits exceeded (in accordance with export licensing restrictions)
    VARIANCE                = 8,    // Variance exceeds limits
    RESIDUALS               = 9,    // Residuals are too large
    /* 10-12  Reserved */
    INTEGRITY_WARNING       = 13,   // Large residuals make position unreliable
    /* 14-17  Reserved */
    PENDING                 = 18,   // When a FIX position command is entered, the receiver computes its own position and determines if the fixed position is valid
    INVALID_FIX             = 19,   // The fixed position, entered using the FIX position command, is not valid
    UNAUTHORIZED            = 20,   // Position type is unauthorized
    /* 21-21  Reserved */
    INVALID_RATE            = 22,   // The selected logging rate is not supported for this solution type.
};

/* Table 91 on page 501 */
enum PosVelType {
    NONE                    = 0,    // No solution
    FIXEDPOS                = 1,    // Position has been fixed by the FIX position command or by position averaging.
    FIXEDHEIGHT             = 2,    // Position has been fixed by the FIX height or FIX auto command or by position averaging
    /* 3-7  Reserved */
    DOPPLER_VEOCITY         = 8,    // Velocity computed using instantaneous Doppler
    /* 9-15 Reserved */
    SINGLE                  = 16,   // Solution calculated using only data supplied by the GNSS satellites
    PSRDIFF                 = 17,   // Solution calculated using pseudorange differential (DGPS, DGNSS) corrections
    WAAS                    = 18,   // Solution calculated using corrections from an SBAS satellite
    PROPAGATED              = 19,   // Propagated by a Kalman filter without new observations
    /* 20-31 Reserved */
    L1_FLOAT                = 32,   // Single-frequency RTK solution with unresolved, float carrier phase ambiguities
    /* 33-33 Reserved */
    NARROW_FLOAT            = 34,   // Multi-frequency RTK solution with unresolved, float carrier phase ambiguitie
    /* 35-47 Reserved */
    L1_INT                  = 48,   // Single-frequency RTK solution with carrier phase ambiguities resolved to integers
    WIDE_INT                = 49,   // Multi-frequency RTK solution with carrier phase ambiguities resolved to wide-lane integers
    NARROW_INT              = 50,   // Multi-frequency RTK solution with carrier phase ambiguities resolved to narrrow-lane integers
    RTK_DIRECT_INS          = 51,   // RTK status where the RTK filter is directly initialized from the INS filter
    INS_SBAS                = 52,   // INS position, where the last applied position update used a GNSS solution computed using corrections from an SBAS (WAAS) solution
    INS_PSRSP               = 53,   // INS position, where the last applied position update used a single point GNSS (SINGLE) solution
    INS_PSRDIFF             = 54,   // INS position, where the last applied position update used a pseudorange differential GNSS (PSRDIFF) solution
    INS_RTKFLOAT            = 55,   // INS position, where the last applied position update used a floating ambiguity RTK (L1_FLOAT or NARROW_FLOAT) solution
    INS_RTKFIXED            = 56,   // INS position, where the last applied position update used a fixed integer ambiguity RTK (L1_INT, WIDE_INT or NARROW_INT) solution
    /* 57-67 Reserved */
    PPP_CONVERGING          = 68,   // Converging TerraStar-C, TerraStar-C PRO or TerraStar-X solution
    PPP                     = 69,   // Converged TerraStar-C, TerraStar-C PRO or TerraStar-X solution
    OPERATIONAL             = 70,   // Solution accuracy is within UAL operational limit
    WARNING                 = 71,   // Solution accuracy is outside UAL operational limit but within warning limit
    OUT_OF_BOUNDS           = 72,   // Solution accuracy is outside UAL limits
    INS_PPP_CONVERGING      = 73,   // INS position, where the last applied position update used a converging TerraStar-C, TerraStar-C PRO or TerraStar-X PPP (PPP_CONVERGING) solution
    INS_PPP                 = 74,   // INS position, where the last applied position update used a converged TerraStar-C, TerraStar-C PRO or TerraStar-X PPP (PPP) solution
    PPP_BASIC_CONVERGING    = 77,   // Converging TerraStar-L solution
    PPP_BASIC               = 78,   // Converged TerraStar-L solution
    INS_PPP_BASIC_CONVERGING = 79,  // INS position, where the last applied positio TerraStar-L PPP (PPP_BASIC) solution
    INS_PPP_BASIC           = 80,   // INS position, where the last applied position TerraStar-L PPP (PPP_BASIC) solution
};

/* Table 105: "Clock Model Status" on page 526 */
enum ClockModelStatus {
    CLOCK_STATUS__VALID     = 0,    // The clock model is valid
    CLOCK_STATUS__CONVERGING = 1,   // The clock model is near validity
    CLOCK_STATUS__ITERATING = 2,    // The clock model is iterating towards validity
    CLOCK_STATUS__INVALID   = 3,    // The clock model is not valid
};

/* Offsets of fields in the binary header */
#define OFFSET_HEADER_LENGTH    (3)
#define OFFSET_MESSAGE_ID       (4)
#define OFFSET_MESSAGE_TYPE     (6)
#define OFFSET_PORT_ADDRESS     (7)
#define OFFSET_MESSAGE_LENGTH   (8)
#define OFFSET_TIME_STATUS      (13)
#define OFFSET_GPS_WEEK         (14)
#define OFFSET_GPS_TOW          (16)

#endif /* _GPSD_NOVATEL_H_ */
// vim: set expandtab shiftwidth=4
