#!/bin/sh -e

LOG_FILE="novatel-binary-01.log"

rm -f $LOG_FILE
#cat > $LOG_FILE << EOF
## Name: NovAtel OEM7
## Chipset: ??
## Submitter: "Francois Retief" <fgretief@dragonflyaerospace.com>
##
## Binary data generated from documentation and manuals (see 'build-logs.sh' script)
##
## mode binary
#EOF

# LOG Response Header
echo -en "\xAA\x44\x12\x1c\x01\x00\x82\x20\x06\x00\x00\x00\xFF\xB4\xEE\x04\x60\x5A\x05\x13\x00\x00\x4C\x00\xFF\xFF\x5A\x80" >> $LOG_FILE
# LOG Response Data
echo -en "\x01\x00\x00\x00\x4F\x4B" >> $LOG_FILE
# Checksum
echo -en "\xDA\x86\x88\xEC" >> $LOG_FILE

# BESTPOSB Header
echo -en "\xAA\x44\x12\x1C\x2A\x00\x02\x20\x48\x00\x00\x00\x90\xB4\x93\x05\xB0\xAB\xB9\x12\x00\x00\x00\x00\x45\x61\xBC\x0A" >> $LOG_FILE
# BESTPOSB Data
echo -en "\x00\x00\x00\x00\x10\x00\x00\x00\x1B\x04\x50\xB3\xF2\x8E\x49\x40\x16\xFA\x6B\xBE\x7C\x82\x5C\xC0\x00\x60\x76\x9F" >> $LOG_FILE
echo -en "\x44\x9F\x90\x40\xA6\x2A\x82\xC1\x3D\x00\x00\x00\x12\x5A\xCB\x3F\xCD\x9E\x98\x3F\xDB\x66\x40\x40\x00\x30\x30\x30" >> $LOG_FILE
echo -en "\x00\x00\x00\x00\x00\x00\x00\x00\x0B\x0B\x00\x00\x00\x06\x00\x03" >> $LOG_FILE
# Checksum
echo -en "\x42\xDC\x4C\x48" >> $LOG_FILE

echo -en "\x00" >> $LOG_FILE
exit 0
