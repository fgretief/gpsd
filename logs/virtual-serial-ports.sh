#!/bin/sh -e
PORT_A=/tmp/ttyS35
PORT_B=/tmp/ttyS36
echo "Virtual Serial Ports:  ${PORT_A}  <->  ${PORT_B}"
exec socat pty,raw,echo=0,link=${PORT_A}  pty,raw,echo=0,link=${PORT_B}
#../buildtmp/gpsd/./gpsd -D 5 -S 9999 -N -n ${PORT_A}
#../buildtmp/gpsd/./gpsd -D 5 -S 9999 -N -n /tmp/ttyS35
